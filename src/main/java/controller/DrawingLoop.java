package controller;


import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;
import model.Ball;
import model.Character;
import model.Wall;
import view.Platform;

import java.util.ArrayList;
import java.util.Optional;

public class DrawingLoop implements Runnable {

    private Platform platform;
    private int frameRate;
    private float interval;
    private boolean running;
    private int counting;
    private int timerCount;
    private String Endgamealert;

    private float power_std;
    private int xmaxspeed_char_std;
    private int ymaxspeed_char_std;
    private int speed_ball_timer;
    private int speed_char_timer;
    private boolean access_speed_ball = false;
    private boolean access_speed_char = false;

    public DrawingLoop(Platform platform) {
        this.platform = platform;
        power_std = this.platform.getBall().getPower();
        xmaxspeed_char_std = this.platform.getCharacterList().get(1).getxMaxVelocity();
        ymaxspeed_char_std = this.platform.getCharacterList().get(1).getyMaxVelocity();
        frameRate = 60;
        interval = 1000.0f / frameRate; // 1000 ms = 1 second
        running = true;
        timerCount = 60;
        counting = 0;
    }

    private void check_character_collision(ArrayList<Character> characterList) {
        for (Character character : characterList ) {
            character.checkReachGameWall();
            character.checkReachHighest();
            character.checkReachFloor();
        }
        for (Character cA : characterList) {
            for (Character cB : characterList) {
                if (cA != cB) {
                    if (cA.getBoundsInParent().intersects(cB.getBoundsInParent())) {
                        cA.collided(cB);
                        cB.collided(cA);
                        return;
                    }
                }
            }
        }
    }
    private void check_ball_collision(Ball ball, ArrayList<Character> characterList, Wall wall){
        ball.checkReachFloor(characterList);
        ball.checkHitWall(wall);
        for (Character c:characterList) {
            ball.check_hit_character(c,platform);
        }
    }

    private void checkBallSkill(ArrayList<Character> characterList){
        for (Character character : characterList ){
            if(platform.getKeys().isPressed(character.getSlowBallKey()) && access_speed_ball == false){
                access_speed_ball = true;
                System.out.println("access");
                platform.getBall().setPower(13);
            }
            if(platform.getKeys().isPressed(character.getFastBallKey()) && access_speed_ball == false){
                access_speed_ball = true;
                System.out.println("access");
                platform.getBall().setPower(22);
            }
        }
    }
    private void checkCharacterSpeedSkill(ArrayList<Character> characterList){
        if(platform.getKeys().isPressed(characterList.get(0).getSlowEnemyKey()) && access_speed_char == false){
            access_speed_char = true;
            System.out.println("access");
            platform.getCharacterList().get(1).setSpeed(3,12);
        }
        if(platform.getKeys().isPressed(characterList.get(1).getSlowEnemyKey()) && access_speed_char == false){
            access_speed_char = true;
            System.out.println("access");
            platform.getCharacterList().get(0).setSpeed(3,12);
        }
        if(platform.getKeys().isPressed(characterList.get(0).getFastCharKey()) && access_speed_char == false){
            access_speed_char = true;
            System.out.println("access");
            platform.getCharacterList().get(0).setSpeed(10,20);
        }
        if(platform.getKeys().isPressed(characterList.get(1).getFastCharKey()) && access_speed_char == false){
            access_speed_char = true;
            System.out.println("access");
            platform.getCharacterList().get(1).setSpeed(10,20);
        }
    }

    private void paint_character(ArrayList<Character> characterList) {
        for (Character character : characterList ) {
            character.repaint();
        }
    }
    private void paint_ball(Ball ball){
        ball.repaint();
    }

    private void checkTime(ArrayList<Character> characterList){
        if (timerCount<=0){
            running = false;
            if(characterList.get(0).getScore()>characterList.get(1).getScore()){
                Endgamealert = "Player 1 is win";
                System.out.println("p1 win");
                running = false;
            }else if(characterList.get(0).getScore()<characterList.get(1).getScore()){
                Endgamealert = "Player 2 is win";
                System.out.println("p2 win");
                running = false;
            }else{
                Endgamealert = "Game Tie";
                System.out.println("game tie");
                running = false;
            }
            javafx.application.Platform.runLater(()->{
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setTitle("Game is end.");
                alert.setHeaderText(Endgamealert);
                alert.setContentText("Are you want to restart or exit?");
                Optional<ButtonType> result = alert.showAndWait();
                if (result.get() == ButtonType.OK){
                    running = true;
                    for (Character c : characterList){
                        c.setScore(0);
                    }
                    System.exit(0);
                    Launcher.getPrimaryStage().close();
                    new Launcher().start(new Stage());
                } else {
                    Launcher.getPrimaryStage().close();
                }
            });
        }
    }

    @Override
    public void run() {
        while (running) {

            float time = System.currentTimeMillis();

            
            check_character_collision(platform.getCharacterList());
            paint_character(platform.getCharacterList());
            checkTime(platform.getCharacterList());
            check_ball_collision(platform.getBall(), platform.getCharacterList(), platform.getWall());
            paint_ball(platform.getBall());
            checkBallSkill(platform.getCharacterList());
            checkCharacterSpeedSkill(platform.getCharacterList());
            System.out.println("Ball POW:"+platform.getBall().getPower());
            if(access_speed_ball == true){
                speed_ball_timer++;
                if(speed_ball_timer >= 300){
                    speed_ball_timer = 0;
                    platform.getBall().setPower(power_std);
                    access_speed_ball = false;
                    System.out.println("3sec.");
                }
            }
            System.out.println("Character 1 SPD"+platform.getCharacterList().get(0).getxMaxVelocity());
            System.out.println("Character 2 SPD"+platform.getCharacterList().get(1).getxMaxVelocity());
            if(access_speed_char == true){
                speed_char_timer++;
                if(speed_char_timer >= 180){
                    speed_char_timer = 0;
                    platform.getCharacterList().get(0).setSpeed(xmaxspeed_char_std,ymaxspeed_char_std);
                    platform.getCharacterList().get(1).setSpeed(xmaxspeed_char_std,ymaxspeed_char_std);
                    access_speed_char = false;
                    System.out.println("3sec.");
                }
            }

            counting++;
            if(counting>=100){
                counting = 0;
                timerCount--;
                javafx.application.Platform.runLater(()->{
                    platform.setTimer(timerCount);
                });
            }

            time = System.currentTimeMillis() - time;

            if (time < interval) {
                try {
                    Thread.sleep((long) (interval - time));
                } catch (InterruptedException ignore) {
                }
            } else {
                try {
                    Thread.sleep((long) (interval - (interval % time)));
                } catch (InterruptedException ignore) {
                }
            }
        }
    }
}
